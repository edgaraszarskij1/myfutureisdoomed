import React, { Component, Fragment } from 'react';
import Card from './Card'
import { shuffle } from './shuffle'
import './App.css';

class App extends Component {
  constructor(props) {
    super(props)
    const newArr = []
    for (let i = 0; i < 9; i++) {
      newArr.push(i)
    }
    shuffle(newArr)
    this.state = { newArr, isFinished: false }
  }

  newGameHandler = () => {
    let newArr = this.state.newArr;
    this.setState(shuffle(newArr))
  }

  isSorted = (array) => {
    for (let i = 0; i < array.length - 1; i++) {
      if (array[i] > array[i + 1]) {
        return false;
      }
    }
    return true;
  }

  handleClick = (id) => {
    let tileIndex = this.state.newArr.findIndex(tile => tile === id)
    let clonedTilesArray = [...this.state.newArr];
    let tileZeroIndex = clonedTilesArray.indexOf(0)
    let size = 3
    if (this.isSorted(clonedTilesArray)) {
      this.setState({ isFinished: true });
      return;
    }
    console.log('ą ', (tileIndex + 1) % size ,' b ', tileIndex % size)
    if ((tileIndex + 1 === tileZeroIndex && (tileIndex + 1) % size > tileIndex % size)
      || (tileIndex - 1 === tileZeroIndex && (tileIndex - 1) % size < tileIndex % size)
      || tileIndex + size === tileZeroIndex || tileIndex - size === tileZeroIndex) {

      this.setState(prevState => {
        let temp = clonedTilesArray[tileIndex];
        clonedTilesArray[tileIndex] = clonedTilesArray[tileZeroIndex];
        clonedTilesArray[tileZeroIndex] = temp;
        return { newArr: clonedTilesArray };
      })
    }
  }

  render() {
    return (
      <Fragment>
        {!this.isSorted(this.state.newArr) ? <div>
          <div className='finished'>New Game</div>
          ><Card newArr={this.state.newArr} handleClick={this.handleClick} />
        </div> :
          <div>
            <div className='finished'>Finished</div>
            <Card newArr={this.state.newArr} handleClick={this.handleClick} />
          </div>}
        <button onClick={this.newGameHandler} style={{ marginLeft: '625px' }}>New Game</button>
      </Fragment>
    );
  }
}

export default App;
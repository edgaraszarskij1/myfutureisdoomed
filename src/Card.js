import React from 'react';
import './Card.css';

function card(props) {
  return (
    <div className='box-border'>
   {props.newArr.map(card => (
     card !==0?
     <div className='card-box'
      key={card} onClick={()=>props.handleClick(card)}>
      {card}
      </div>: <div className='card-white'
      key={card} onClick={()=>props.handleClick(card)}>
      </div>
   ))} 
    </div>
  )
}

export default card